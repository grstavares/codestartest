<?php 

$ip_server = $_SERVER['SERVER_ADDR']; 
$mysql_host = getenv("DB_HOST");
$mysql_user = getenv("DB_USER");
$mysql_password = getenv("DB_PASSWORD");
$mysql_dbname = getenv("DB_NAME");

try {
    $dbh = new PDO("mysql:host=$mysql_host;dbname=$mysql_dbname", $mysql_user, $mysql_password);
    foreach($dbh->query('SELECT VERSION()') as $row) {
        echo "{ 'ServerHost': '$ip_server', 'MySQLVersion': $row[0] }";
    }
    $dbh = null;
} catch (PDOException $e) {
    echo "{ 'ServerHost': '$ip_server', 'MySQLError': $e->getMessage() }";
    die();
}
?>
